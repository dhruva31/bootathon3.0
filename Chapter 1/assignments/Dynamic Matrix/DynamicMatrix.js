//select number of rows
var s1 = document.getElementById("s1");
//select number of rows
var s2 = document.getElementById("s2");
//table1
var tb1 = document.getElementById("tb1");
//table2
var tb2 = document.getElementById("tb2");
//table2 resultant
var tb3 = document.getElementById("tb3");
//adding number of rows to select tag
for (let i = 2; i <= 6; i++) {
    var option = document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);
}
//adding number of cols to select tag
for (let i = 2; i <= 6; i++) {
    var option = document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s2.add(option);
}
//onchange select tag create input matrix
function createinputmat() {
    //deleting table1
    deletetable(tb1);
    //deleting table2
    deletetable(tb2);
    //create input table 1
    createtable(tb1, "a");
    //create input table 2
    createtable(tb2, "b");
}
//delete all the rows
function deletetable(t1) {
    while (t1.rows.length > 0) {
        t1.deleteRow(0);
    }
}
//createtable
function createtable(t1, id) {
    var row = +s1.value;
    var col = +s2.value;
    for (let i = 0; i < row; i++) {
        var t1row = t1.insertRow();
        for (let j = 0; j < col; j++) {
            var t1cell = t1row.insertCell();
            var text = document.createElement("input");
            text.id = id + i + j;
            text.type = "text";
            t1cell.appendChild(text);
        }
    }
}
//button onclick multiply
function multiply() {
    var a = [];
    var b = [];
    var c = [];
    readmat(a, +s1.value, +s1.value, "a");
    readmat(b, +s1.value, +s2.value, "b");
    matmultiply(a, b, c, +s1.value, +s2.value);
    createtable(tb3, "c");
    displayresultant(c, +s1.value, +s2.value, "c");
}
//read the matrix
function readmat(a, row, col, id) {
    for (let i = 0; i < row; i++) {
        a[i] = [];
        for (let j = 0; j < col; j++) {
            let t1 = document.getElementById(id + i + j);
            a[i][j] = +t1.value;
        }
    }
}
//multiply two matrix
function matmultiply(a, b, c, row, col) {
    for (let i = 0; i < row; i++) {
        c[i] = [];
        for (let k = 0; k < col; k++) {
            c[i][k] = 0;
            for (let j = 0; j < col; j++) {
                c[i][k] += a[i][j] * b[j][k];
            }
        }
    }
}
//display resultant
function displayresultant(a, row, col, id) {
    for (let i = 0; i < row; i++) {
        for (let j = 0; j < col; j++) {
            let t1 = document.getElementById(id + i + j);
            t1.value = a[i][j].toString();
        }
    }
}
//# sourceMappingURL=DynamicMatrix.js.map