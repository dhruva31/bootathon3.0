//select number of rows
var s1 = document.getElementById("s1");
//table1
var tb1 = document.getElementById("tb1");
//paragraph elements
let res = document.getElementById("result");
let ang = document.getElementById("angle");
//adding number of rows to select tag
for (let i = 2; i <= 6; i++) {
    var option = document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);
}
//onchange select tag create input matrix
function createinputmat() {
    //deleting table1
    deletetable(tb1);
    //create input table 1
    createtable(tb1, "a");
}
//delete all the rows
function deletetable(t1) {
    while (t1.rows.length > 0) {
        t1.deleteRow(0);
    }
}
//createtale
function createtable(t1, id) {
    var row = +s1.value;
    var col = 2;
    for (let i = 0; i < row; i++) {
        var t1row = t1.insertRow();
        for (let j = 0; j < col; j++) {
            var t1cell = t1row.insertCell();
            var text = document.createElement("input");
            text.id = id + i + j;
            text.type = "text";
            t1cell.appendChild(text);
        }
    }
}
//button onclick multiply
function resultant() {
    var a = [];
    //reading force and angle
    readmat(a, +s1.value, 2, "a");
    //calculating resultant
    calculate(a);
}
//read the matrix
function readmat(a, row, col, id) {
    //id-> textbook id of matrix
    for (let i = 0; i < row; i++) {
        a[i] = [];
        for (let j = 0; j < col; j++) {
            let t1 = document.getElementById(id + i + j);
            a[i][j] = +t1.value;
        }
    }
}
//calculate resultant of force
function calculate(a) {
    var sx = 0;
    var sy = 0;
    for (let i = 0; i < +s1.value; i++) {
        sx += a[i][0] = Math.cos(Math.PI / 180 * a[i][1]); //summation of x
        sy += a[i][0] = Math.sin(Math.PI / 180 * a[i][1]); //summation of y
    }
    var result = Math.sqrt(Math.pow(sx, 2) + Math.pow(sy, 2)); //resultant magnitude
    var angl = Math.atan2(sy, sx); //resultant angle
    console.log("Mag", result);
    console.log("angle", angl * 180 / Math.PI);
    res.innerHTML = result.toString();
    ang.innerHTML = angl.toString();
}
//# sourceMappingURL=DynamicFunction.js.map