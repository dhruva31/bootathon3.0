declare var graphline;

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];
datapoints1.push({ x: 10, y: 10 });
datapoints1.push({ x: 20, y: 10 });
datapoints1.push({ x: 20, y: 20 });
datapoints1.push({ x: 10, y: 20 });
datapoints1.push({ x: 10, y: 10 });

datapoints2.push({ x: 10, y: 10 });
datapoints2.push({ x: 20, y: 20 });
datapoints2.push({ x: 30, y: 30 });
datapoints2.push({ x: 40, y: 40 });
datapoints2.push({ x: 50, y: 50 });

var data = [];
data.push({
    type: "spline",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x)",
    markerSize: 1,
    dataPoints: datapoints1
})
                        
data.push({
    type: "line",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x1)",
    markerSize: 1,
    dataPoints: datapoints2
})                 

graphline("l1", data, "", "",100,-100);