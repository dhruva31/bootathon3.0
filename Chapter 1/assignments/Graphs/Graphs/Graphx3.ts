declare var drawgraph2;         //Framework function
declare var drawgraph;          //Framework function

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];

for(var i=1;i<=10;i++)
{
    datapoints1.push({ x: i, y: i * i * i });
    datapoints2.push({ x: i, y: i * i * i + i });
}
drawgraph("l1", datapoints1, "x axis", "x cube");

function draw1()
{
    drawgraph2("l2", datapoints1, datapoints2, "X-axis", "Y-axis","comparision","x^3","x^3+x");
}
 