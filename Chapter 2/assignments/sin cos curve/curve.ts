class Curve
{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;
    public amp = 85;

    constructor(x:number, y:number, canvas:HTMLCanvasElement, context:CanvasRenderingContext2D)
    {
    this.x = x;
    this.y = y;
    this.canvas = canvas;
    this.context = context;
    }

    draw()                                   
    {
        console.log("HELLO")                           //drawing of axes is initialised
        this.context.beginPath();
        this.context.moveTo(this.x,this.y-85);
        this.context.lineTo(this.x,this.y+85);
        this.context.moveTo(this.x-85,this.y);
        this.context.lineTo(this.x+360,this.y);
        this.context.lineWidth = 2;                     //axes thickness
        this.context.strokeStyle = 'black';             //axes colour
        this.context.stroke()                           //sketch the axis and end

        var xo:number = this.x;
        var yo:number = this.y;

        this.context.beginPath();                     //start of sine curve
        this.context.lineTo(xo , yo + this.amp * Math.sin(Math.PI / 180 ));
        for(var i = 0; i<360; i++)
        {
            this.context.lineTo(xo + i , yo + this.amp * Math.sin(Math.PI * i / 180));
        }
        this.context.lineWidth = 2.5;                                 //curve thickness
        this.context.strokeStyle = 'violet';                         //curve colour
        this.context.stroke()                                         //sketch curve and end

        this.context.beginPath();                         //start of cosine curve
        this.context.lineTo(xo , yo + this.amp * Math.cos(Math.PI / 180 ));
        for(var i = 0; i<360; i++)
        {
            this.context.lineTo(xo + i , yo + this.amp * Math.cos(Math.PI * i / 180));
        }
        this.context.lineWidth = 2.5;                                  //curve thickness
        this.context.strokeStyle = 'red';                                 //curve colour
        this.context.stroke()                                              //sketch curve and end
    }
}