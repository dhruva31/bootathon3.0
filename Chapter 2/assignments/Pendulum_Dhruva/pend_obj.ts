//parametric line
class Line
{
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private refX: number;
    private refY: number;
    private ang:number;
    private len:number;

    constructor(x:number,y:number,ang:number,len:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.refX = x;
        this.refY = y;
        this.ang = ang;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }

    drawpara()
    {
        console.log("hello");
        this.context.beginPath();
        this.context.moveTo(this.refX,this.refY);
        this.context.lineTo(this.refX+(this.len*Math.cos(this.ang)),this.refY+(this.len*Math.sin(this.ang)));
        this.context.strokeStyle = "blue";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}

//Circle
class Circle
{
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private refX: number;
    private refY: number;
    private r: number;

    constructor(x:number,y:number,r:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.refX = x;
        this.refY = y;
        this.r = r;
        this.canvas = canvas;
        this.context = context;
    }

    drawpara()
    {
        console.log("hello");
        this.context.beginPath();
        this.context.arc(this.refX,this.refY,this.r,0,2*Math.PI,true);
        this.context.strokeStyle = "blue";
        this.context.fillStyle = "red";
        this.context.fill();
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}

//pendulum
class Pendulum
{
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private refX: number;
    private refY: number;
    private ang:number;
    private len:number;
    private base: Line;
    private string: Line;
    private bob: Circle;

    constructor(x:number,y:number,ang:number,len:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.refX = x;
        this.refY = y;
        this.ang = ang;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }

    drawfirst()
    {
        this.base = new Line(this.refX-50,this.refY,0,100,this.canvas,this.context);//using line class above
        this.base.drawpara();
        this.string = new Line(this.refX,this.refY,Math.PI/2,this.len,this.canvas,this.context);
        this.string.drawpara();
        this.bob = new Circle(this.refX,this.refY+this.len,this.len/5,this.canvas,this.context);//using circle class to drw bob
        this.bob.drawpara();
    }


}