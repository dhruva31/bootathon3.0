//parametric line
class Line {
    constructor(x, y, ang, len, canvas, context) {
        this.refX = x;
        this.refY = y;
        this.ang = ang;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }
    drawpara() {
        console.log("hello");
        this.context.beginPath();
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX + (this.len * Math.cos(this.ang)), this.refY + (this.len * Math.sin(this.ang)));
        this.context.strokeStyle = "blue";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
//Circle
class Circle {
    constructor(x, y, r, canvas, context) {
        this.refX = x;
        this.refY = y;
        this.r = r;
        this.canvas = canvas;
        this.context = context;
    }
    drawpara() {
        console.log("hello");
        this.context.beginPath();
        this.context.arc(this.refX, this.refY, this.r, 0, 2 * Math.PI, true);
        this.context.strokeStyle = "blue";
        this.context.fillStyle = "blue";
        this.context.fill();
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
//and
class And {
    constructor(x, y, r, canvas, context) {
        this.refX = x;
        this.refY = y;
        this.rr = r;
        this.canvas = canvas;
        this.context = context;
    }
    drawfirst() {
        context.beginPath();
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX, this.refY + 50);
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX - 25, this.refY);
        this.context.moveTo(this.refX, this.refY + 50);
        this.context.lineTo(this.refX - 25, this.refY + 50);
        this.context.moveTo(this.refX + 25, this.refY + 25);
        this.context.lineTo(this.refX + 50, this.refY + 25);
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX, this.refY + 50);
        this.context.arc(this.refX, this.refY + 25, this.rr, 3 * Math.PI / 2, Math.PI / 2, false);
        this.context.strokeStyle = "blue";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
class Not {
    constructor(x, y, canvas, context) {
        this.refX = x;
        this.refY = y;
        this.canvas = canvas;
        this.context = context;
    }
    drawfirst() {
        context.beginPath();
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX, this.refY + 50);
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX - 25, this.refY);
        this.context.moveTo(this.refX, this.refY + 50);
        this.context.lineTo(this.refX - 25, this.refY + 50);
        this.context.moveTo(this.refX, this.refY);
        this.context.lineTo(this.refX + 25, this.refY + 25);
        // this.context.lineTo(this.refX,this.refY+50);  //triangle complete
        this.context.arc(this.refX + 30, this.refY + 25, 5, 0, 2 * Math.PI);
        this.context.moveTo(this.refX + 35, this.refY + 25);
        this.context.lineTo(this.refX + 60, this.refY + 25); // line after circle
        this.context.moveTo(this.refX + 25, this.refY + 25);
        this.context.lineTo(this.refX, this.refY + 50);
        /* this.context.moveTo(this.refX,this.refY);
         this.context.lineTo(this.refX,this.refY+50);*/
        this.context.strokeStyle = "blue";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
//# sourceMappingURL=gates_obj.js.map